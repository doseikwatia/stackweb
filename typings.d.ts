/* SystemJS module definition */
declare var module: NodeModule;
declare function require(path: string): any;
interface NodeModule {
  id: string;
}
