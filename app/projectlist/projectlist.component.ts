import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProjectModel, TaskModel, IProjectModel, ITaskModel} from '../models';
import { MockModels} from '../mock-projects';
@Component({
  selector: 'app-projectlist',
  templateUrl: './projectlist.component.html',
  styleUrls: ['./projectlist.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectlistComponent implements OnInit {

  constructor() { }

  projects: IProjectModel[]=  MockModels.Projects;
  currentProjectID: number;

  ngOnInit() {
  }
  ResetEdit() {
    const currentProject: any = this.projects.filter((pjt) => pjt.Id === this.currentProjectID);
     currentProject.forEach((pjt) => {pjt.edit = false; });
  }
  OnProjectClicked(project: IProjectModel) {
    if (project.Id !== this.currentProjectID) {
      this.ResetEdit();
    }
//    MockModels.SelectProject(project.Id);
    this.currentProjectID = project.Id;
  }
  OnKeyUp(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.ResetEdit();
    }
  }
}
