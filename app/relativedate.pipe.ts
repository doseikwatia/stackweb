import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'relativedate'
})
export class RelativedatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const humanize = require('humanize-duration');
    const date: Date = <Date>value;
    const now = new Date();
    const suffix = (date.getTime() < now.getTime()) ? ' ago' : ' to go';
    const humanized: string = humanize(date.getTime() - now.getTime(), { units: ['y', 'mo', 'd'] , round: true});
    return  humanized + suffix;
  }

}
