import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TasklistComponent } from './tasklist/tasklist.component';
import { ProjectlistComponent } from './projectlist/projectlist.component';


const routes: Routes = [{path: 'tasks/:pjtid', component: TasklistComponent, outlet: 'tasklist'}];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
