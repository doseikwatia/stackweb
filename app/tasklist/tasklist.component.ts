import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ProjectModel, TaskModel } from '../models';
import { ActivatedRoute } from '@angular/router';
import { MockModels } from '../mock-projects';
@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TasklistComponent implements OnInit {
  [x: string]: any;

  constructor(private route: ActivatedRoute) { }
  @Input()
  tasks: TaskModel[];

  ngOnInit() {
    this.route.paramMap.subscribe(map => {
      this.tasks = MockModels.getTasks(+map.get('pjtid'));
    });
  }

}
