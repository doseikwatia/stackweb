import {TaskModel, ProjectModel, IProjectModel, ITaskModel} from './models';
import { ProjectlistComponent } from './projectlist/projectlist.component';

export class MockModels {
  private static  pjtifcs: IProjectModel[]= [
    {
      Id: 1,
      Name: 'MyGo Project', Description: 'Description',
      Tasks: [
          {Id: 1, Name: 'Task1', Description: 'TaskDesc', DueDate: new Date(), Priority: 1, Progress: 1, Tasks: []},
          {Id: 2, Name: 'Task2', Description: 'TaskDesc', DueDate: new Date(), Priority: 2, Progress: 15, Tasks: []}
      ]
    },
    {
      Id: 2,
      Name: 'Machine Learing Project', Description: 'Description',
      Tasks: [
          {Id: 1, Name: 'Algorithm1', Description: 'TaskDesc', DueDate: new Date(), Priority: 3, Progress: 1, Tasks: []},
          {Id: 2, Name: 'Algorithm2', Description: 'TaskDesc', DueDate: MockModels.NextWeek(), Priority: 5, Progress: 15, Tasks: [
            {Id: 1, Name: 'Task1', Description: 'TaskDesc', DueDate: new Date(), Priority: 1, Progress: 1, Tasks: []},
            {Id: 2, Name: 'Task2', Description: 'TaskDesc', DueDate: new Date(), Priority: 2, Progress: 15, Tasks: []}
          ]}
      ]
    }
  ];
  public static NextWeek(): Date {
    const result = new Date();
    result.setMonth(11);
    return result;
  }
  public static get Projects(): ProjectModel[] {
    return this.pjtifcs.map((pjt) =>  {
      return ProjectModel.GetProjectModel(pjt);
    });
  }

  public static getTasks(id: Number): TaskModel[] {
      const projects: IProjectModel[] = MockModels.pjtifcs.filter((pjt) =>  {
        return pjt.Id === id;
      });
      const result: TaskModel[] = (projects.length === 0 ? [] : projects[0].Tasks ).map((task) => TaskModel.GetTaskModel(task));
    return result;
  }
}
