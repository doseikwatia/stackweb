import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import {TaskModel} from '../models';
import {TaskeditorComponent} from '../taskeditor/taskeditor.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-taskitem',
  templateUrl: './taskitem.component.html',
  styleUrls: ['./taskitem.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TaskitemComponent implements OnInit {
  constructor(private modalService: NgbModal) { }
  @Input()
  task: TaskModel;
  edit= false;
  ngOnInit() {
  }
  OnDoubleClicked() {
    TaskeditorComponent.show(this.task, this.modalService);
  }
}
