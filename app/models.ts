export interface IProjectModel {
  Id: number;
  Name: string;
  Description: string;
  Tasks: ITaskModel[];
}
export interface ITaskModel {
  Id: number;
  Name: string;
  Description: string;
  DueDate: Date;
  Progress: number;
  Tasks: ITaskModel[];
  Priority: number;
}
export class TaskModel implements ITaskModel {
    public Id: number;
    public Name: string;
    public Description: string;
    public DueDate: Date;
    public Progress: number;
    public Priority: number;
    public Tasks: TaskModel[];
    constructor() {

    }
    public static GetTaskModel(taskifc: ITaskModel): TaskModel {
      const result: TaskModel = new TaskModel();
      result.Id = taskifc.Id;
      result.Name = taskifc.Name;
      result.Description = taskifc.Description;
      result.DueDate = taskifc.DueDate;
      result.Progress = taskifc.Progress;
      result.Priority = taskifc.Priority;
      result.Tasks = taskifc.Tasks.map((ifc) =>  {
          return TaskModel.GetTaskModel(ifc);
      });
      return result;
    }
}
export class ProjectModel implements IProjectModel {
    public Id: number;
    public Name: string;
    public Description: string;
    public Tasks: ITaskModel[];
    constructor() {

    }
    public static GetProjectModel(pjtifc: IProjectModel): ProjectModel {
      const result: ProjectModel = new ProjectModel();
      result.Id = pjtifc.Id;
      result.Name = pjtifc.Name;
      result.Description = pjtifc.Description;
      result.Tasks = pjtifc.Tasks.map((taskifc) =>  {
        return TaskModel.GetTaskModel(taskifc);
      });
      return result;
    }
}
