import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProjectlistComponent } from './projectlist/projectlist.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { AppRoutingModule } from './app-routing.module';
import { TaskitemComponent } from './taskitem/taskitem.component';
import { TaskeditorComponent } from './taskeditor/taskeditor.component';
import { RelativedatePipe } from './relativedate.pipe';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    ProjectlistComponent,
    TasklistComponent,
    TaskitemComponent,
    TaskeditorComponent,
    RelativedatePipe,
  ],
  imports: [
    BrowserModule, FormsModule, AppRoutingModule, NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [TaskeditorComponent]
})
export class AppModule { }
