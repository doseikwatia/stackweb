import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import {TaskModel} from '../models';
import {NgbModal, ModalDismissReasons, NgbModalRef, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ParenthesizedTypeNode } from 'typescript';
@Component({
  selector: 'app-taskeditor',
  template: '' + `
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{task.Name}}</h5>
          <button type="button" class="close" (click) = "close('close')" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label for="task-name-input" class="col-2 col-form-label">Name</label>
            <div class="col-10">
              <input class="form-control" type="text" [(ngModel)]="task.Name"
                id="task-name-input">
            </div>
          </div>

          <div class="form-group row">
            <label for="task-priority-input" class="col-2 col-form-label">Priority</label>
            <div class="col-4">
              <input class="form-control" type="number" [(ngModel)]="task.Priority" id="task-priority-input">
            </div>
            <label for="task-dueduate-input" class="col-2 col-form-label">Due Date</label>
            <div class="col-4">
              <input class="form-control" type="date" [(ngModel)]="duedate" id="task-dueduate-input">
            </div>

          </div>
          <div class="form-group row">
            <label for="task-progress" class="col-2 col-form-label">Progress</label>
            <div class="col-4">
              <input class="form-control" type="number" [(ngModel)]="task.Progress" id="task-progress">
            </div>
            <div class="col-6"></div>
          </div>
          <div class="form-group row">
            <label for="task-description-textarea"
              class="col-2 col-form-label">Description</label>
            <div class="col-10">
              <textarea class="form-control" id="task-description-textarea" row="3"  [(ngModel)]="task.Description" ></textarea>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"
            (click)="close('save')">Save</button>
          <button type="button" class="btn btn-secondary"
            data-dismiss="modal" (click)="close('close')">Close</button>
        </div>
      </div>
`,
  styleUrls: ['./taskeditor.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TaskeditorComponent implements OnInit {

  constructor() {

  }
  @Input()
  model: TaskModel;
  task: TaskModel;
  modalinstance: NgbModalRef;

  public static show(model: TaskModel, modalService: NgbModal) {
    const dialog = modalService.open(TaskeditorComponent, {size: 'lg'});
    const instance = <TaskeditorComponent> dialog.componentInstance;
    instance.model = model;
    instance.modalinstance = dialog;
    dialog.result.then((result) =>  {
      if (result === 'save') {
        instance.model.Name = instance.task.Name;
        instance.model.Description = instance.task.Description;
        instance.model.DueDate = instance.task.DueDate;
        instance.model.Progress = instance.task.Progress;
        instance.model.Priority = instance.task.Priority;
      }

    }, (reason) => {

    });
}
  public get duedate(): string{
    let result = '';
    if (this.task !== null || this.task !== undefined) {
      const date = new Date(this.task.DueDate);
      result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate()) ;
    }
    return result;
  }
  public set duedate(dateStr: string) {
    if (this.task !== null || this.task !== undefined) {
      const parsedDate = new Date(dateStr);
      const date = new Date(parsedDate.setDate(parsedDate.getDate() + 1));
      this.task.DueDate = date;
    }
  }
  ngOnInit() {
      this.task = <TaskModel>JSON.parse(JSON.stringify(this.model));
  }
  close(data: string) {
    this.modalinstance.close(data);
  }
  dismiss(data: string) {
    this.modalinstance.dismiss(data);
  }
}
